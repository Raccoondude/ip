from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('pled/', views.pled, name='pled'),
]
