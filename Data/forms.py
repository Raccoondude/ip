from django import forms

class PledForm(forms.Form):
    Type_here =  forms.CharField(widget=forms.Textarea(attrs={"rows":5, "cols":20}))
