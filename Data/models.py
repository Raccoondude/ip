from django.db import models

# Create your models here.

class Info(models.Model):
    Info_ID = models.AutoField(primary_key=True)
    Info_IP = models.CharField(max_length=20)
    Info_data = models.TextField(max_length=1000)
    def __str__(self):
        return self.Info_IP

class Pled(models.Model):
    Pled_ID = models.AutoField(primary_key=True)
    Pled_data = models.TextField(max_length=100)
    def __str__(self):
        return str(self.Pled_ID)

class Crime(models.Model):
    Crime_ID = models.IntegerField(default=0)
    Crime_desc = models.TextField(max_length=1000)
    Crime_name = models.CharField(max_length=25)
    def __str__(self):
        return str(self.Crime_ID)
