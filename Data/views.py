from ipware import get_client_ip
from django.shortcuts import render
import requests
from .models import *
from .forms import *
from django.http import HttpResponse, HttpResponseRedirect
# Create your views here.

def index(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[-1].strip()
    else:
        ip = request.META.get('REMOTE_ADDR')
    crime = Crime.objects.get(Crime_ID=0)
    form = PledForm()
    URL = "http://api.ipstack.com/" + ip + "?access_key=5938991a408411f0ecefe83957b27974"
    r = requests.get(url = URL)
    data = r.json()
    data = str(data)
    OwO = Info(Info_IP=ip, Info_data=data)
    OwO.save()
    return render(request, 'Data/index.html', {'crime':crime, 'ip':ip, 'form':form, 'info':data})

def pled(request):
    if request.method == "POST":
        MyForm = PledForm(request.POST)
        if MyForm.is_valid():
            pledOwO = MyForm.cleaned_data['Type_here']
            UwU = Pled(Pled_data=pledOwO)
            UwU.save()
            return HttpResponseRedirect("/")
